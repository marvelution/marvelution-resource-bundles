This project features a Maven Resource Bundle for Marvelution projects

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/MARVADMIN>

Continuous Builder
==================
<https://marvelution.atlassian.net/builds/browse/MARVADMIN-RESBUN>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
